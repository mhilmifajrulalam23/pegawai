@extends('layout.main')
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Data Departemen</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active">Departemen</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ route('departemen.create') }}" class="btn btn-success">Tambah</a></h5>

                            <!-- DataTables Table -->
                            <div class="table-responsive">
                                <table id="departemenTable" class="display">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Departemen</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($departemen as $data)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $data->nama_departemen }}</td>
                                            <td>
                                                <a href="{{ route('departemen.edit', ['id' => $data->id]) }}" class="btn btn-primary mb-2"><i class='bx bxs-edit'> Edit</i></a>
                                                <button onclick="confirmDelete({{ $data->id }})" class="btn btn-danger mb-2"><i class="bx bx-trash"></i> Hapus</button>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#departemenTable').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true
        });
    });

    function confirmDelete(departemenId) {
        Swal.fire({
            title: "Konfirmasi Hapus Data",
            text: "Apakah kamu yakin ingin menghapus data departemen?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
            if (result.isConfirmed) {
                deleteDepartemen(departemenId);
            }
        });
    }

    function deleteDepartemen(departemenId) {
        $.ajax({
            url: '/departemen/delete/' + departemenId,
            type: 'DELETE',
            data: {
                _token: '{{ csrf_token() }}'
            },
            success: function(response) {
                Swal.fire({
                    title: 'Sukses',
                    text: response.success,
                    icon: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'OK'
                }).then(() => {
                    // Refresh halaman setelah berhasil hapus
                    location.reload();
                });
            },
            error: function(xhr) {
                Swal.fire({
                    title: 'Error',
                    text: 'Terjadi kesalahan saat menghapus data.',
                    icon: 'error',
                    confirmButtonColor: '#d33',
                    confirmButtonText: 'OK'
                });
            }
        });
    }

    @if(session('success'))
    Swal.fire({
        title: 'Sukses',
        text: '{{ session('success') }}',
        icon: 'success',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'OK'
    });
    @endif
</script>

@endsection
