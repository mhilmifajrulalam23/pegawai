@extends('layout.main')
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        <h1>Data Pegawai</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active">Pegawai</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><a href="{{ route('pegawai.create') }}" class="btn btn-success">Tambah</a></h5>

                            <!-- DataTables Table -->
                            <div class="table-responsive">
                                <table id="pegawaiTable" class="display">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Jenis Kelamin</th>
                                            <th>Departemen</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pegawai as $data)
                                            <tr id="pegawai-{{ $data->id }}">
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $data->nama_pegawai }}</td>
                                                <td>{{ $data->jenis_kelamin }}</td>
                                                <td>{{ $data->departemen->nama_departemen }}</td>
                                                <td>
                                                    <a href="{{ route('pegawai.detail', ['id' => $data->id]) }}" class="btn btn-warning mb-2"><i class='bx bxs-user-detail'> Detail</i></a>
                                                    <a href="{{ route('pegawai.edit', ['id' => $data->id]) }}" class="btn btn-primary mb-2"><i class='bx bxs-edit'> Edit</i></a>
                                                    <button onclick="confirmDelete({{ $data->id }})" class="btn btn-danger mb-2"><i class="bx bx-trash"></i> Hapus</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</main>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function() {
        $('#pegawaiTable').DataTable({
            "paging": true,
            "searching": true,
            "ordering": true,
            "info": true
        });
    });

    function confirmDelete(pegawaiId) {
        Swal.fire({
            title: "Konfirmasi Hapus Data",
            text: "Apakah kamu yakin ingin menghapus data pegawai?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, hapus!'
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: '/pegawai/delete/' + pegawaiId,
                    type: 'DELETE',
                    data: {
                        _token: '{{ csrf_token() }}'
                    },
                    success: function(response) {
                        if (response.success) {
                            $('#pegawai-' + pegawaiId).remove();
                            Swal.fire({
                                title: 'Sukses',
                                text: 'Data pegawai berhasil dihapus',
                                icon: 'success',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'OK'
                            }).then(() => {
                                // Refresh halaman setelah berhasil hapus
                                location.reload();
                            });
                        } else {
                            Swal.fire({
                                title: 'Gagal',
                                text: 'Data pegawai gagal dihapus',
                                icon: 'error',
                                confirmButtonColor: '#3085d6',
                                confirmButtonText: 'OK'
                            });
                        }
                    },
                    error: function(xhr) {
                        Swal.fire({
                            title: 'Error',
                            text: 'Gagal menghapus data pegawai.',
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'OK'
                        });
                    }
                });
            }
        });
    }

    @if(session('success'))
        Swal.fire({
            title: 'Sukses',
            text: '{{ session('success') }}',
            icon: 'success',
            confirmButtonColor: '#3085d6',
            confirmButtonText: 'OK'
        });
    @endif
</script>
@endsection
