<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function index(){
        return view('register');
    }

    public function register_proses(Request $request){
        $request->validate([
            'name'  => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
        ]);

        $data['name']       = $request->name;
        $data['email']      = $request->email;
        $data['password']   = Hash::make($request->password) ;

        User::create($data);

        $daftar = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        if(Auth::attempt($daftar)){
            return redirect()->route('dashboard');
        }else{
            return redirect()->route('login')->with('failed','Email dan Password tidak sesuai')->withInput();
        }
    }
}
