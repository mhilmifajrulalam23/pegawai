<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartemenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('departemens')->insert([
            [
                'nama_departemen' => 'Pengembangan Produk',
            ],
            [
                'nama_departemen' => 'Teknik',
            ],
            [
                'nama_departemen' => 'Teknologi Informasi',
            ],
            [
                'nama_departemen' => 'Sumber Daya Manusia',
            ],
            [
                'nama_departemen' => 'Keuangan dan Akuntansi',
            ],
            [
                'nama_departemen' => 'Pemasaran',
            ],
            [
                'nama_departemen' => 'Penjualan',
            ],
            [
                'nama_departemen' => 'Layanan Pelanggan',
            ],
            [
                'nama_departemen' => 'Penelitian dan Pengembangan',
            ],
            [
                'nama_departemen' => 'Hukum dan Kepatuhan',
            ]
        ]);
    }
}
